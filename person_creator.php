<!DOCTYPE html>
<html lang="sk">
<head>
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=0.5">
    <meta charset="UTF-8">
    <title>Slovensko <3</title>

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/form-template.css">

    <script src="js/person_form_controller.js"></script>



</head>
<body>

<nav class="head">
    <h1>Slovensko na olympiádach</h1>
</nav>

<div class="menu">
    <div class="wrapper top">
        <div id="image-wrapper">
            <a href="http://wt118.fei.stuba.sk/404.html">
                <img id="emblem" src="resources/images/emblem.png" alt="emblem">
            </a>
        </div>
        <div id="a-wrapper">
            <span ><a href="index.php">Víťazi</a></span>
            <span ><a href="top_results.php">Top10</a></span>
        </div>
    </div>
</div>

<main>
    <form action="person_uploader.php" method="post">
        <h2>Vytvor osobu</h2>
        <label>
            Meno:<br>
            <input type="text" id="name-input" name="name-input" required>
        </label><br>
        <label>
            Priezvisko:<br>
            <input type="text" id="surname-input" name="surname-input" required>
        </label><br><br><br>

        <label>
            Dátum narodenia:<br>
            <input type="date" id="birth-date-input" name="birth-date-input" max="<?php echo date("Y-m-d"); ?>" required>
        </label><br>
        <label>
            Mesto narodenia:<br>
            <input type="text" id="birth-place-input" name="birth-place-input" required>
        </label><br>
        <label>
            Krajina narodenia:<br>
            <input type="text" id="birth-country-input" name="birth-country-input" required>
        </label><br><br><br>

        <label>
            Mŕtvy
            <input type="checkbox" id="death-checkbox" name="death-checkbox" value="mrtvy">
        </label><br>
        <div id="death-inputs">
            <label>
                Dátum úmrtia:<br>
                <input type="date" id="death-date-input" name="death-date-input" max="<?php echo date("Y-m-d"); ?>" >
            </label><br>
            <label>
                Mesto úmrtia:<br>
                <input type="text" id="death-place-input" name="death-place-input">
            </label><br>
            <label>
                Krajina úmrtia:<br>
                <input type="text" id="death-country-input" name="death-country-input" >
            </label><br>
        </div>

        <div id="button-wrapper">
            <button type="submit">Pridať osobu</button>
        </div>
    </form>
</main>

</body>
</html>