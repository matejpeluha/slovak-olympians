<!DOCTYPE html>
<html lang="sk">
<head>
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=0.5">
    <meta charset="UTF-8">
    <title>Slovensko <3</title>

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>


</head>
<body>

<nav class="head">
    <h1>Slovensko na olympiádach</h1>
</nav>

<div class="menu">
    <div class="wrapper top">
        <div id="image-wrapper">
            <a href="http://wt118.fei.stuba.sk/404.html">
                <img id="emblem" src="resources/images/emblem.png" alt="emblem">
            </a>
        </div>
        <div id="a-wrapper">
            <span ><a href="index.php">Víťazi</a></span>
            <span ><a href="top_results.php">Top10</a></span>
        </div>
    </div>
</div>

<main>
    <?php
    require_once('config.php');

    function transformDate($date){
        $dateArray = explode("-", $date);
        $date = "";
        for ($index = 2; $index >= 0; $index--){
            $date .= $dateArray[$index];
            if($index != 0)
                $date .= ".";
        }
        return $date;
    }

    if( !isset($_POST["person-select"]) || !isset($_POST["oh-select"]) || !isset($_POST["discipline-input"])
        || !isset($_POST["placing-input"]) ){
        echo "<h2>Zle načítané údaje z formulára</h2><div id='icon-wrapper'><i class='far fa-times-circle fa-10x'></i></div>";
        return;
    }

    $personId = $_POST["person-select"];
    $ohId = $_POST["oh-select"];
    $discipline = $_POST["discipline-input"];
    $placing = $_POST["placing-input"];


    $connection = new PDO("mysql:host=".DB_HOST."; dbname=".DB_NAME, DB_USER, DB_PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    if ($connection == null){
        echo "<h2>NEMOŽNO ZAPISOVAŤ DO DATABÁZY</h2>";
        return;
    }


    $query = $connection->prepare("INSERT INTO umiestnenia(person_id, oh_id, placing, discipline)
                                                VALUES(?, ?, ?, ?)");
    $query->execute(array($personId, $ohId, $placing, $discipline));

    echo "<h2>Osoba pridaná</h2><div id='icon-wrapper'><i class='far fa-check-circle fa-10x'></i></div>";

    ?>

</main>

</body>
</html>
