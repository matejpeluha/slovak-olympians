<!DOCTYPE html>
<html lang="sk">
<head>
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=0.5">
    <meta charset="UTF-8">
    <title>Slovensko <3</title>

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>


</head>
<body>

<nav class="head">
    <h1>Slovensko na olympiádach</h1>
</nav>

<div class="menu">
    <div class="wrapper top">
        <div id="image-wrapper">
            <a href="http://wt118.fei.stuba.sk/404.html">
                <img id="emblem" src="resources/images/emblem.png" alt="emblem">
            </a>
        </div>
        <div id="a-wrapper">
            <span ><a href="index.php">Víťazi</a></span>
            <span ><a href="top_results.php">Top10</a></span>
        </div>
    </div>
</div>

<main>
    <?php
    require_once('config.php');

    function transformDate($date){
        $dateArray = explode("-", $date);
        $date = "";
        for ($index = 2; $index >= 0; $index--){
            $date .= $dateArray[$index];
            if($index != 0)
                $date .= ".";
        }
        return $date;
    }


    if( !isset($_GET["id"]) ){
        echo "<h2>Osoba nenájdená</h2><div id='icon-wrapper'><i class='far fa-times-circle fa-10x'></i></div>";
        return;
    }

    $id = $_GET["id"];


    $connection = new PDO("mysql:host=".DB_HOST."; dbname=".DB_NAME, DB_USER, DB_PASSWORD);
    $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    if ($connection == null){
        echo "<h2>NEMOŽNO MAZAŤ Z DATABÁZY</h2>";
        return;
    }

    $query = $connection->prepare("DELETE FROM osoby WHERE osoby.id=" . $id);
    $query->execute();

    echo "<h2>Osoba upravená</h2><div id='icon-wrapper'><i class='far fa-check-circle fa-10x'></i></div>";
    ?>

</main>

</body>
</html>

