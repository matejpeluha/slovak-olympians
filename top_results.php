<!DOCTYPE html>
<html lang="sk">
<head>
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=0.5">
    <meta charset="UTF-8">
    <title>Slovensko <3</title>

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/table-template.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>


</head>
<body>

<nav class="head">
    <h1>Slovensko na olympiádach</h1>
</nav>

<div class="menu">
    <div class="wrapper top">
        <div id="image-wrapper">
            <a href="http://wt118.fei.stuba.sk/404.html">
                <img id="emblem" src="resources/images/emblem.png" alt="emblem">
            </a>
        </div>
        <div id="a-wrapper">
            <span ><a href="index.php">Víťazi</a></span>
            <span class="active-menu"><a href="top_results.php">Top10</a></span>
        </div>
    </div>
</div>

<main>
    <?php
    require_once "php/classes/controllers/TopSportsmenController.class.php";

    $topSportsmenController = new TopSportsmenController();
    $topSportsmenController->createTopTable();

    ?>

    <div id="button-wrapper">
        <form action="person_creator.php">
            <button type="submit">Pridať osobu</button>
        </form>
        <form action="result_creator.php">
            <button type="submit">Pridať úspech</button>
        </form>
    </div>
</main>

</body>
</html>