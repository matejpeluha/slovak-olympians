<?php

if(!defined('__ROOT__')) {
    define('__ROOT__', dirname(dirname(dirname(dirname(__FILE__)))));
}

require_once(__ROOT__.'/config.php');
require_once(__ROOT__ . '/php/classes/entities/Result.class.php');

class ResultsController
{
    private $connection;

    public function __construct()
    {
        $this->connection = new PDO("mysql:host=".DB_HOST."; dbname=".DB_NAME, DB_USER, DB_PASSWORD);
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function createSelectInput(){
        if ($this->connection == null){
            echo "<p>SELECT NEMOŽNO VYTVORIŤ<p>";
            return;
        }

        $query = $this->connection->prepare("SELECT oh.id AS ohId, oh.type, oh.year, oh.city FROM oh ORDER BY oh.year DESC ");
        $query->execute();
        $query->setFetchMode(PDO::FETCH_CLASS, "Result");

        $allOlympics = $query->fetchAll();

        echo "<label for='oh-select'>Olympiáda:</label><br><select id='oh-select' name='oh-select'>";

        foreach ($allOlympics as $olympic)
            echo $olympic->getOption();

        echo "</select><br>";
    }

    public function createResultTable()
    {
        if ($this->connection == null){
            echo "TABUĽKU NEMOŽNO NAČÍTAŤ";
            return;
        }

        $query = $this->connection->prepare("SELECT osoby.id, osoby.name, osoby.surname,
                                                oh.city, umiestnenia.discipline, oh.type, 
                                                oh.year FROM osoby JOIN umiestnenia ON osoby.id=umiestnenia.person_id
                                                 AND umiestnenia.placing = 1 
                                                JOIN oh ON oh.id=umiestnenia.oh_id  
                                                ORDER BY `osoby`.`id` ASC");

        $query->execute();
        $query->setFetchMode(PDO::FETCH_CLASS, "Result");

        $allResults = $query->fetchAll();

        $this->printResultTable($allResults);

    }

    private function printResultTable($allResults){
        echo "<table id='result-table'><thead><tr><th>Meno</th><th class='ordering' data-order='0'>Priezvisko <i class='fas fa-sort'></i></th>
                <th>Disciplína</th><th>Miesto</th><th class='ordering' data-order='0'>Olympiáda <i class='fas fa-sort'></i></th>
                <th class='ordering' data-order='0'>Rok <i class='fas fa-sort'></i></th></tr></thead><tbody>";

        foreach($allResults as $result) {
            echo $result->getRow();
        }

        echo "</tbody></table>";

    }

}