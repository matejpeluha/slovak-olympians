<?php

require_once(__ROOT__.'/config.php');
require_once(__ROOT__ . '/php/classes/entities/PersonalResult.class.php');

class PersonalResultsController
{

    private $connection;

    public function __construct()
    {
        $this->connection = new PDO("mysql:host=".DB_HOST."; dbname=".DB_NAME, DB_USER, DB_PASSWORD);
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function createResultTable()
    {
        if ($this->connection == null){
            echo "TABUĽKU NEMOŽNO NAČÍTAŤ";
            return;
        }

        if(!isset($_GET["id"])) {
            return;
        }

        $personID = $_GET["id"];
        if($personID === "") {
            return;
        }

        $query = $this->connection->prepare("SELECT umiestnenia.placing, umiestnenia.discipline, oh.type, oh.year, oh.city 
                                                    FROM umiestnenia JOIN oh 
                                                    ON oh.id = umiestnenia.oh_id AND umiestnenia.person_id = ".$personID);

        $query->execute();
        $query->setFetchMode(PDO::FETCH_CLASS, "PersonalResult");

        $allResults = $query->fetchAll();

        $this->printResultTable($allResults);

    }

    private function printResultTable($allResults){
        echo "<table id='result-table'><thead><tr><th>Olympiáda</th><th>Rok</th>
                <th>Miesto</th><th>Disciplína</th><th>Umiestnenie</th></tr></thead><tbody>";

        foreach($allResults as $result) {
            echo $result->getRow();
        }

        echo "</tbody></table>";

    }

}