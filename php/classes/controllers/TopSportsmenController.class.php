<?php

define('__ROOT__', dirname(dirname(dirname(dirname(__FILE__)))));
require_once(__ROOT__.'/config.php');
require_once(__ROOT__ . '/php/classes/entities/TopSportsman.class.php');

class TopSportsmenController
{
    private $connection;

    public function __construct()
    {
        $this->connection = new PDO("mysql:host=".DB_HOST."; dbname=".DB_NAME, DB_USER, DB_PASSWORD);
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function createTopTable()
    {
        if ($this->connection == null){
            echo "TABUĽKU NEMOŽNO NAČÍTAŤ";
            return;
        }

        $query = $this->connection->prepare("SELECT osoby.id, osoby.name, osoby.surname, COUNT(case when umiestnenia.placing = 1 OR umiestnenia.placing = 2 OR umiestnenia.placing = 3 then 1 else null end) AS medalsCount, COUNT(case when umiestnenia.placing = 1 then 1 else null end) AS goldCount, COUNT(case when umiestnenia.placing = 2 then 1 else null end) AS silverCount, COUNT(case when umiestnenia.placing = 3 then 1 else null end) AS bronzeCount FROM osoby JOIN umiestnenia ON osoby.id=umiestnenia.person_id GROUP BY osoby.id ORDER BY `goldCount` DESC,  `silverCount` DESC, `bronzeCount` DESC, `medalsCount` DESC LIMIT 10");

        $query->execute();
        $query->setFetchMode(PDO::FETCH_CLASS, "TopSportsman");

        $allTop = $query->fetchAll();

        $this->printResultTable($allTop);

    }

    private function printResultTable($allTop){
        echo "<table><thead><tr><th>Meno</th><th>Priezvisko</th>
                <th>Zlato</th><th>Striebro</th><th>Bronz</th><th>Počet medailí</th><th>Upraviť</th><th>Zmazať</th></tr></thead><tbody>";

        foreach($allTop as $top) {
            echo $top->getRow();
        }

        echo "</tbody></table>";

    }

}