<?php

define('__ROOT__', dirname(dirname(dirname(dirname(__FILE__)))));
require_once(__ROOT__.'/config.php');
require_once(__ROOT__ . '/php/classes/entities/Person.class.php');


class PersonController
{
    private $connection;

    public function __construct()
    {
        $this->connection = new PDO("mysql:host=".DB_HOST."; dbname=".DB_NAME, DB_USER, DB_PASSWORD);
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function createSelectInput(){
        if ($this->connection == null){
            echo "<p>SELECT NEMOŽNO VYTVORIŤ<p>";
            return;
        }

        $query = $this->connection->prepare("SELECT * FROM osoby");

        $query->execute();
        $query->setFetchMode(PDO::FETCH_CLASS, "Person");

        $allPersons = $query->fetchAll();

        echo "<label for='person-select'>Osoba:</label><br><select id='person-select' name='person-select'>";
        foreach ($allPersons as $person)
            echo $person->getOption();

        echo "</select><br>";

    }

    public function createPersonPage(){
        if ($this->connection == null){
            echo "TABUĽKU NEMOŽNO NAČÍTAŤ";
            return;
        }

        if(!isset($_GET["id"])) {
            echo "<h2>Neexistujúci športovec</h2>";
            return;
        }

        $id = $_GET["id"];
        if($id === "") {
            echo "<h2>Neexistujúci športovec</h2>";
            return;
        }


        $query = $this->connection->prepare("SELECT * FROM osoby WHERE osoby.id = " . $id);

        $query->execute();
        $query->setFetchMode(PDO::FETCH_CLASS, "Person");

        $allPersons = $query->fetchAll();
        if(sizeof($allPersons) < 1){
            echo "<h2>Neexistujúci športovec</h2>";
            return;
        }

        $person = $allPersons[0];

        $person->printPerson();
    }

    public function createFilledForm(){
        if ($this->connection == null){
            echo "<h2>TABUĽKU NEMOŽNO NAČÍTAŤ<h2>";
            return;
        }

        if(!isset($_GET["id"])) {
            echo "<h2>Neexistujúci športovec</h2>";
            return;
        }

        $id = $_GET["id"];
        if($id === "") {
            echo "<h2>Neexistujúci športovec</h2>";
            return;
        }

        $query = $this->connection->prepare("SELECT * FROM osoby WHERE osoby.id = " . $id);

        $query->execute();
        $query->setFetchMode(PDO::FETCH_CLASS, "Person");

        $allPersons = $query->fetchAll();
        if(sizeof($allPersons) < 1){
            echo "<h2>Neexistujúci športovec</h2>";
            return;
        }

        $person = $allPersons[0];

        $person->printFilledForm();
    }

}

