<?php


class Person
{
    private $id;
    private $name;
    private $surname;
    private $birth_day;
    private $birth_place;
    private $birth_country;
    private $death_day;
    private $death_place;
    private $death_country;

    public function printPerson(){
        echo "<h2>$this->name $this->surname</h2>";

        echo "<div id='info'>";

        echo "<p><em class='bold'>Narodenie:</em>";
        echo "$this->birth_day, $this->birth_place, $this->birth_country</p>";

        echo "<p><em class='bold'>Úmrtie:</em>";

        if($this->death_day === "")
            echo "-----------------------------------------</p>";
        else
            echo "$this->death_day, $this->death_place, $this->death_country</p>";

        echo "</div>";
    }

    public function getOption(){
        $birthYear = explode(".", $this->birth_day)[2];
        $deathYear = "";
        if($this->death_day !== "") {
            $deathYear= explode(".", $this->death_day)[2];
        }


        return "<option data-birth-year='$birthYear' data-death-year='$deathYear' value='$this->id'>$this->id. $this->name $this->surname
                ($birthYear-$deathYear)</option>";
    }

    public function printFilledForm(){
        echo "<form action='person_edit_uploader.php' method='post'>";
        echo "<h2>$this->id. $this->name $this->surname</h2>";
        echo $this->getHiddenIdInput();
        echo $this->getNameInput();
        echo $this->getSurnameInput();
        echo "<br><br>";
        echo $this->getBirthDateInput();
        echo $this->getBirthPlaceInput();
        echo $this->getBirthCountryInput();
        echo "<br><br>";
        echo $this->getDeathCheckbox();
        echo "<div id='death-inputs'>";
        echo $this->getDeathDateInput();
        echo $this->getDeathPlaceInput();
        echo $this->getDeathCountryInput();
        echo "</div>";
        echo $this->getFormSubmitButton();
        echo "</form>";
    }

    private function getHiddenIdInput(){
        return " <input type='hidden' id='id-input' name='id-input' value='$this->id'>";
    }

    private function getNameInput(){
        return "<label>
                    Meno:<br>
                    <input value='$this->name' type='text' id='name-input' name='name-input' required>
                </label><br>";
    }

    private function getSurnameInput(){
        return "<label>
                    Priezvisko:<br>
                    <input value='$this->surname' type='text' id='surname-input' name='surname-input' required>
                </label><br>";
    }

    private function getBirthDateInput(){
        $birthTime = strtotime($this->birth_day);
        $birthDate = date('Y-m-d',$birthTime);
        return "<label>
                Dátum narodenia:<br>
                <input value='$birthDate' type='date' id='birth-date-input' name='birth-date-input' max='" . date("Y-m-d") . "' required>
              </label><br>";
    }

    private function getBirthPlaceInput(){
        return "<label>
                Mesto narodenia:<br>
                <input value='$this->birth_place' type='text' id='birth-place-input' name='birth-place-input' required>
              </label><br>";
    }

    private function getBirthCountryInput(){
        return "<label>
                Krajina narodenia:<br>
                <input value='$this->birth_country' type='text' id='birth-country-input' name='birth-country-input' required>
              </label><br>";
    }

    private function getDeathCheckbox(){
        if($this->death_day && $this->death_country && $this->death_place) {

            return "<label>
                        Mŕtvy
                        <input type='checkbox' id='death-checkbox' name='death-checkbox' value='mrtvy' checked>
                    </label><br>";
        }
        else{
            return "<label>
                        Mŕtvy
                        <input type='checkbox' id='death-checkbox' name='death-checkbox' value='mrtvy'>
                    </label><br>";
        }
    }

    private function getDeathDateInput(){
        if($this->death_day) {
            $deathTime = strtotime($this->death_day);
            $deathDate = date('Y-m-d', $deathTime);
            return "<label>
                    Dátum úmrtia:<br>
                    <input value='$deathDate' type='date' id='death-date-input' name='death-date-input' max='" . date('Y-m-d') . "' required>
                </label><br>";
        }

        else {
            return "<label>
                    Dátum úmrtia:<br>
                    <input value='' type='date' id='death-date-input' name='death-date-input' max='" . date('Y-m-d') . "' >
                </label><br>";
        }
    }

    private function getDeathPlaceInput(){
        if($this->death_place) {
            return "<label>
                    Mesto úmrtia:<br>
                    <input value='$this->death_place' type='text' id='death-place-input' name='death-place-input' required>
                </label><br>";
        }
        else{
            return "<label>
                    Mesto úmrtia:<br>
                    <input value='' type='text' id='death-place-input' name='death-place-input' >
                </label><br>";
        }
    }

    private function getDeathCountryInput(){
        if($this->death_country) {
            return "<label>
                    Krajina úmrtia:<br>
                    <input value='$this->death_country' type='text' id='death-country-input' name='death-country-input' required>
                </label><br>";
        }
        else{
            return "<label>
                    Krajina úmrtia:<br>
                    <input value='' type='text' id='death-country-input' name='death-country-input'>
                </label><br>";
        }
    }

    private function getFormSubmitButton(){
        return "<div id='button-wrapper'>
                    <button type='submit'>Upraviť osobu</button>
                </div>";
    }

}