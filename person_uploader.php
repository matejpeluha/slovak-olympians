<!DOCTYPE html>
<html lang="sk">
<head>
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=0.5">
    <meta charset="UTF-8">
    <title>Slovensko <3</title>

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>


</head>
<body>

<nav class="head">
    <h1>Slovensko na olympiádach</h1>
</nav>

<div class="menu">
    <div class="wrapper top">
        <div id="image-wrapper">
            <a href="http://wt118.fei.stuba.sk/404.html">
                <img id="emblem" src="resources/images/emblem.png" alt="emblem">
            </a>
        </div>
        <div id="a-wrapper">
            <span ><a href="index.php">Víťazi</a></span>
            <span ><a href="top_results.php">Top10</a></span>
        </div>
    </div>
</div>

<main>
    <?php
    require_once('config.php');

        function transformDate($date){
            $dateArray = explode("-", $date);
            $date = "";
            for ($index = 2; $index >= 0; $index--){
                $date .= $dateArray[$index];
                if($index != 0)
                    $date .= ".";
            }
            return $date;
        }

        if( !isset($_POST["name-input"]) || !isset($_POST["surname-input"]) || !isset($_POST["birth-date-input"])
            || !isset($_POST["birth-place-input"]) || !isset($_POST["birth-country-input"]) ){
            echo "<h2>Zle načítané údaje z formulára</h2><div id='icon-wrapper'><i class='far fa-times-circle fa-10x'></i></div>";
            return;
        }

        $name = $_POST["name-input"];
        $surname = $_POST["surname-input"];
        $birthDate = transformDate($_POST["birth-date-input"]);
        $birthPlace = $_POST["birth-place-input"];
        $birthCountry= $_POST["birth-country-input"];
        $deathDate = "";
        $deathPlace = "";
        $deathCountry = "";


        if(isset($_POST["death-checkbox"]) &&
            ( !isset($_POST["death-date-input"]) || !isset($_POST["death-place-input"]) || !isset($_POST["death-country-input"]))){

            echo "<h2>Zle načítané údaje z formulára</h2><div id='icon-wrapper'><i class='far fa-times-circle fa-10x'></i></div>";
            return;
        }

        else if(isset($_POST["death-checkbox"]) &&
            ( isset($_POST["death-date-input"]) && isset($_POST["death-place-input"]) && isset($_POST["death-country-input"]))){

            $deathDate = transformDate($_POST["death-date-input"]);
            $deathPlace = $_POST["death-place-input"];
            $deathCountry = $_POST["death-country-input"];
        }

        $connection = new PDO("mysql:host=".DB_HOST."; dbname=".DB_NAME, DB_USER, DB_PASSWORD);
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        if ($connection == null){
            echo "<h2>NEMOŽNO ZAPISOVAŤ DO DATABÁZY</h2>";
            return;
        }

        try{
            $query = $connection->prepare("INSERT INTO osoby(name, surname, birth_day, birth_place,
                                               birth_country, death_day, death_place, death_country )
                                                VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
            $query->execute(array($name, $surname, $birthDate, $birthPlace, $birthCountry, $deathDate, $deathPlace, $deathCountry));
        }catch (Exception $exception){
            echo "<h2>Osoba s týmto menom už je zapísaná</h2>
                        <div id='icon-wrapper'><i class='far fa-times-circle fa-10x'></i></div></main></body></html>";
            exit();
        }
        echo "<h2>Osoba pridaná</h2><div id='icon-wrapper'><i class='far fa-check-circle fa-10x'></i></div>";
        ?>

</main>

</body>
</html>
