<!DOCTYPE html>
<html lang="sk">
<head>
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=0.5">
    <meta charset="UTF-8">
    <title>Slovensko <3</title>

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/person.css">
    <link rel="stylesheet" href="css/table-template.css">

</head>
<body>

<nav class="head">
    <h1>Slovensko na olympiádach</h1>
</nav>

<div class="menu">
    <div class="wrapper top">
        <div id="image-wrapper">
            <a href="http://wt118.fei.stuba.sk/404.html">
                <img id="emblem" src="resources/images/emblem.png" alt="emblem">
            </a>
        </div>
        <div id="a-wrapper">
            <span ><a href="index.php">Víťazi</a></span>
            <span ><a href="top_results.php">Top10</a></span>
        </div>
    </div>
</div>

<main>
    <?php
    require_once "php/classes/controllers/PersonController.class.php";
    require_once "php/classes/controllers/PersonalResultsController.class.php";

    $personController = new PersonController();
    $personController->createPersonPage();

    $personalResultsController = new PersonalResultsController();
    $personalResultsController->createResultTable();

    ?>

</main>

</body>
</html>