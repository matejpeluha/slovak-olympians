
window.addEventListener("load", function (){
    const birthDate = document.getElementById("birth-date-input");
    birthDate.addEventListener('input', validDate);

    const deathDate = document.getElementById("death-date-input");
    deathDate.addEventListener('input', validDate);

    const deathCheckbox = document.getElementById("death-checkbox");
    deathCheckbox.addEventListener("change", changeDeath)

    const deathInputs = document.getElementById("death-inputs");
    if(deathCheckbox.checked === true)
        deathInputs.style.display = "block";
    else
        deathInputs.style.display = "none";
})



function validDate(e){
    const day = new Date( e.target.value ).getUTCDay();
    const month = new Date( e.target.value ).getUTCMonth();
    const year = new Date( e.target.value ).getUTCFullYear();

    // Days in JS range from 0-6 where 0 is Sunday and 6 is Saturday

    if( day >= 30 && month === 2 ){
        e.target.setCustomValidity("Neplatný dátum");
    }
    else if( day >= 31 && (month === 4 || month === 6 || month === 9 || month === 11) ){
        e.target.setCustomValidity("Neplatný dátum");
    }
    else {
        e.target.setCustomValidity('')
    }

    validateBirthWithDeath();
}

function validateBirthWithDeath(){
    const birthDate = document.getElementById("birth-date-input");
    const deathDate = document.getElementById("death-date-input");

    if(birthDate.value === "" || deathDate.value === "")
        birthDate.setCustomValidity("");
    else if(birthDate.value >= deathDate.value && deathDate.required === true) {
        birthDate.setCustomValidity("Dátum narodenia musí byť skôr, než dátum úmrtia");
    }
    else
        birthDate.setCustomValidity("");
}

function changeDeath(e){
    if(e.target.checked === true)
        showDeathInputs();
    else if(e.target.checked === false)
        hideDeathInputs();
}

function showDeathInputs(){
    const deathDateInput = document.getElementById("death-date-input");
    const deathPlaceInput = document.getElementById("death-place-input");
    const deathCountryInput = document.getElementById("death-country-input");

    deathDateInput.required = true;
    deathPlaceInput.required = true;
    deathCountryInput.required = true;

    const deathInputs = document.getElementById("death-inputs");
    deathInputs.style.display = "block";

    validateBirthWithDeath();
}

function hideDeathInputs(){
    const deathDateInput = document.getElementById("death-date-input");
    const deathPlaceInput = document.getElementById("death-place-input");
    const deathCountryInput = document.getElementById("death-country-input");

    deathDateInput.required = false;
    deathPlaceInput.required = false;
    deathCountryInput.required = false;

    const deathInputs = document.getElementById("death-inputs");
    deathInputs.style.display = "none";

    validateBirthWithDeath();
}


