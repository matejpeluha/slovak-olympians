let surnameOrder = 0;
let yearOrder = 0;
let ohOrder = 0;

window.addEventListener("load", setSorting);

function setSorting(){
    const surname = document.getElementsByClassName("ordering")[0];
    surname.addEventListener("click", sortTableBySurname);

    const year = document.getElementsByClassName("ordering")[2];
    year.addEventListener("click", sortTableByYear);


    const oh = document.getElementsByClassName("ordering")[1];
    oh.addEventListener("click", sortTableByOlympics);
}

function sortTableBySurname() {
    let table, rows, switching, i, x, y, shouldSwitch, order, orderingIcon, secondIcon, thirdIcon, icons;

    icons = document.getElementsByClassName("fas");

    orderingIcon = icons[0];
    secondIcon = icons[1];
    thirdIcon = icons[2]

    order = document.getElementsByClassName("ordering")[0].getAttribute("data-order");

    orderingIcon.removeAttribute("class");
    secondIcon.removeAttribute("class");
    thirdIcon.removeAttribute("class");

    secondIcon.setAttribute("class", "fas fa-sort");
    thirdIcon.setAttribute("class", "fas fa-sort");

    const orderings = document.getElementsByClassName("ordering");
    orderings[1].setAttribute("data-order", "0");
    orderings[2].setAttribute("data-order", "0");

    if(order === "0") {
        order = "1";
        orderingIcon.setAttribute("class", "fas fa-sort-down");
    }
    else if(order === "1") {
        order = "-1";
        orderingIcon.setAttribute("class", "fas fa-sort-up");
    }
    else if(order === "-1") {
        order = "0";
        orderingIcon.setAttribute("class", "fas fa-sort");
        document.getElementsByClassName("ordering")[0].setAttribute("data-order", order);
        resetTable()
        return;
    }

    document.getElementsByClassName("ordering")[0].setAttribute("data-order", order);

    table = document.getElementById("result-table");
    switching = true;


    while (switching) {
        switching = false;
        rows = table.getElementsByTagName("tbody")[0].rows;

        for (i = 0; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("td")[1].getElementsByTagName("a")[0];
            y = rows[i + 1].getElementsByTagName("td")[1].getElementsByTagName("a")[0];

            const xText = x.innerHTML.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
            const yText = y.innerHTML.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase()

            if (order === "-1" && xText > yText) {
                shouldSwitch = true;
                break;
            }

            if (order === "1" && xText < yText) {
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}


function sortTableByYear(){
    let table, rows, switching, i, x, y, shouldSwitch, order, orderingIcon, firstIcon, secondIcon, icons;

    icons = document.getElementsByClassName("fas");

    firstIcon = icons[0];
    secondIcon = icons[1];
    orderingIcon = icons[2]

    order = document.getElementsByClassName("ordering")[2].getAttribute("data-order");

    orderingIcon.removeAttribute("class");
    firstIcon.removeAttribute("class");
    secondIcon.removeAttribute("class");

    firstIcon.setAttribute("class", "fas fa-sort");
    secondIcon.setAttribute("class", "fas fa-sort");

    const orderings = document.getElementsByClassName("ordering");
    orderings[0].setAttribute("data-order", "0");
    orderings[1].setAttribute("data-order", "0");

    if(order === "0") {
        order = "1";
        orderingIcon.setAttribute("class", "fas fa-sort-down");
    }
    else if(order === "1") {
        order = "-1";
        orderingIcon.setAttribute("class", "fas fa-sort-up");
    }
    else if(order === "-1") {
        order = "0";
        orderingIcon.setAttribute("class", "fas fa-sort");
        document.getElementsByClassName("ordering")[2].setAttribute("data-order", order);
        resetTable()
        return;
    }

    document.getElementsByClassName("ordering")[2].setAttribute("data-order", order);

    table = document.getElementById("result-table");
    switching = true;


    while (switching) {
        switching = false;
        rows = table.getElementsByTagName("tbody")[0].rows;

        for (i = 0; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("td")[5];
            y = rows[i + 1].getElementsByTagName("td")[5];

            const xNumber = parseInt(x.innerHTML);
            const yNumber = parseInt(y.innerHTML);

            if (order === "-1" && xNumber > yNumber) {
                shouldSwitch = true;
                break;
            }

            if (order === "1" && xNumber < yNumber) {
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}


function sortTableByOlympics(){
    let table, rows, switching, i, xOh, yOh, xYear, yYear, shouldSwitch, order, orderingIcon, firstIcon, thirdIcon, icons;

    icons = document.getElementsByClassName("fas");

    firstIcon = icons[0];
    orderingIcon = icons[1];
    thirdIcon = icons[2]

    orderingIcon.removeAttribute("class");
    firstIcon.removeAttribute("class");
    thirdIcon.removeAttribute("class");

    firstIcon.setAttribute("class", "fas fa-sort");
    thirdIcon.setAttribute("class", "fas fa-sort");

    order = document.getElementsByClassName("ordering")[1].getAttribute("data-order");
    const orderings = document.getElementsByClassName("ordering");
    orderings[0].setAttribute("data-order", "0");
    orderings[2].setAttribute("data-order", "0");


    if(order === "0") {
        order = "1";
        orderingIcon.setAttribute("class", "fas fa-sort-down");
    }
    else if(order === "1") {
        order = "-1";
        orderingIcon.setAttribute("class", "fas fa-sort-up");
    }
    else if(order === "-1") {
        order = "0";
        orderingIcon.setAttribute("class", "fas fa-sort");
        document.getElementsByClassName("ordering")[1].setAttribute("data-order", order);
        resetTable()
        return;
    }

    document.getElementsByClassName("ordering")[1].setAttribute("data-order", order);

    table = document.getElementById("result-table");
    switching = true;


    while (switching) {
        switching = false;
        rows = table.getElementsByTagName("tbody")[0].rows;

        for (i = 0; i < (rows.length - 1); i++) {
            shouldSwitch = false;


            xOh = rows[i].getElementsByTagName("td")[4];
            yOh = rows[i + 1].getElementsByTagName("td")[4];
            xYear = rows[i].getElementsByTagName("td")[5];
            yYear = rows[i + 1].getElementsByTagName("td")[5];

            const xText = xOh.innerHTML.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
            const yText = yOh.innerHTML.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase()
            const xNumber = parseInt(xYear.innerHTML);
            const yNumber = parseInt(yYear.innerHTML);


            if (order === "-1" && xText > yText) {
                shouldSwitch = true;
                break;
            }

            else if (order === "1" && xText < yText) {
                shouldSwitch = true;
                break;
            }

            else if (order === "-1" && xText === yText && xNumber > yNumber) {
                shouldSwitch = true;
                break;
            }

            else if (order === "1" && xText === yText && xNumber < yNumber) {
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}



function resetTable(){
    let table, rows, switching, i, x, y, shouldSwitch;

    table = document.getElementById("result-table");
    switching = true;

    while (switching) {
        switching = false;
        rows = table.getElementsByTagName("tbody")[0].rows;

        for (i = 0; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i];
            y = rows[i + 1];

            const xNumber = parseInt(x.getAttribute("data-id"));
            const yNumber = parseInt(y.getAttribute("data-id"));

            if ( xNumber > yNumber) {
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }

}

