window.addEventListener("load", setValidation);

function setValidation(){
    validateYear();

    const personSelect = document.getElementById("person-select");
    personSelect.addEventListener('input', validateYear);

    const ohSelect = document.getElementById("oh-select");
    ohSelect.addEventListener('input', validateYear);
}

function validateYear(){
    const personSelect = document.getElementById("person-select");
    const selectedPersonOption = personSelect.options[personSelect.selectedIndex];

    const ohSelect = document.getElementById("oh-select");
    const selectedOhOption = ohSelect.options[ohSelect.selectedIndex];

    const birthYear = selectedPersonOption.getAttribute("data-birth-year");
    const deathYear = selectedPersonOption.getAttribute("data-death-year");
    const ohYear = selectedOhOption.getAttribute("data-year");

    if(birthYear >= ohYear)
        ohSelect.setCustomValidity("Olympiáda sa musí odohrávať počas života športovca.");
    else if(deathYear !== "" && deathYear < ohYear)
        ohSelect.setCustomValidity("Olympiáda sa musí odohrávať počas života športovca.");
    else
        ohSelect.setCustomValidity("");
}